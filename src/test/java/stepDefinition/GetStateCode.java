package stepDefinition;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GetStateCode {

	RequestSpecification httpRequest;
	Response response;

	@Given("that I setup the {string}")
	public void that_I_setup_the(String baseURI) {
		RestAssured.baseURI = baseURI;
	}

	@When("I provided the values of {string}, {string} and {string}")
	public void i_provided_the_values_of_and(String lat, String lon, String key) {
		httpRequest = RestAssured.given();
		response = httpRequest.get("/current?lat=" + lat + "&lon=" + lon + "&key=" + key);
	}

	@Then("I should be able to get the value of the {string}")
	public void i_should_be_able_to_get_the_value_of_the(String stateCode) {

		ResponseBody body = response.getBody();
		String state_code = body.jsonPath().getString("data.state_code");

		System.out.println("State code is " + state_code.replaceAll("[^a-zA-Z]", ""));
	}

}
