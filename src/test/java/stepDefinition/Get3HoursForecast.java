package stepDefinition;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Get3HoursForecast {

	RequestSpecification httpRequest;
	Response response;
	
	@When("I provided the value of {string} and {string}")
	public void i_provided_the_value_of_and(String postalCode, String key) {
		httpRequest = RestAssured.given();
		response = httpRequest.get("/forecast/3hourly?postal_code=" + postalCode + "&key=" + key);
	}


	@Then("I should be able to get the {int} hours forecast")
	public void i_should_be_able_to_get_the_hours_forecast(Integer int1) {
		
		ResponseBody body = response.getBody();
		System.out.println("Response body is " + body.asString());
	    
	}

}
