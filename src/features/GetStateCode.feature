@StatusCode
Feature: Get State COde
  

  @StatusCode
  Scenario Outline: Get State COde
    Given that I setup the "<baseURI>"
    When I provided the values of "<lat>", "<lon>" and "<key>"
    Then I should be able to get the value of the "<stateCode>"

    Examples: 
      | baseURI  | lat | lon  | key | stateCode|
      | https://api.weatherbit.io/v2.0 | 40.730610 | -73.935242  | a81c082596214da7b85a25be338120a9  | NY |