@3HourForecast
Feature: Get 3 hours forecast
  

  @3HourForecast
  Scenario Outline: Get State COde
    Given that I setup the "<baseURI>"
    When I provided the value of "<postalCode>" and "<key>"
    Then I should be able to get the 3 hours forecast

    Examples: 
      | baseURI  | postalCode | key |
      | https://api.weatherbit.io/v2.0 | 10001 | a81c082596214da7b85a25be338120a9 |