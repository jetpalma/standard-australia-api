package au.com.sa.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/features/", glue= {"stepDefinition"}, tags= {"@3HourForecast"}, plugin = { "pretty",
"html:target/cucumber" })
public class TestRunner {
}