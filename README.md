How to build the project
Pre-requisite: Java and apache maven are already installed and configured in your machine.
1. Clone the project using git bash or command prompt
2. Navigate to the project .Ex. cd standard-australia-api
3. run mvn clean install

How to run the scripts
1. Import th project to IDE
2. Open TestRunner.java
3. Change the value of the tags on which test you want to run